# #### Zadanie świąteczne
# 1. Utwórz klasę: `Elf` z atrybutami
# * `id`
# * `name`
# * `level`
# 2. Utwórz klasę: `Gift` z atrybutami
# * `name`
# * `difficulty`
# 3. Utwórz zmienną globalną `Integer christmasCoin`
# 4. Utwórz Menu
import random
import uuid

from Elf import Elf
from Gift import Gift

christmasCoin = 0
list_of_elves = []
list_of_gifts = []


# ##### Dodawanie Elfa
# Podczas dodawania elfa użytkownik zostanie zapytany o imie elfa.
# Domyślnie elf tworzy się z poziomem `1`, id jest generowane (uuid).
# ##### Elf złośnik
# Podczas tworzenia elfa wylosuj liczbe od 0 do 4, jeśli wylosowana liczba to 3, to elf utworzy się z levelem -3.
def add_elf():
    random_num = random.randint(0, 4)

    if (random_num != 3):
        id = str(uuid.uuid4())
        name = input("Podaj imię nowego Elfa ")
        level = 1
        print("Nowy elf będzie bardzo pomocny")
    else:
        id = str(uuid.uuid4())
        name = input("Podaj imię nowego Elfa ")
        level = -3
        print("Uwaga, nowy Elf to złośnik!")

    elf = Elf(id, name, level)

    list_of_elves.append(elf)

    file = open("Fabryka Elfów.txt", "a")
    file.write(elf.id + '\n')
    file.write(elf.name + '\n')
    file.write(str(elf.level) + '\n')
    file.close()
    menu()


# ##### Wyświetlanie Eflów
# Wyświetla wszystkie elfy wraz z ich wszystkimi cechami
def print_elves():
    for elf in list_of_elves:
        print(str(elf) + "\n")
    menu()


##### Wyprodukuj prezent
# Użytkownik zostanie poproszony o nazwe prezentu, poziom trudności wytworzenia przedmiotu.
# Jeśli poziom trudności wytworzenia prezentu jest większa niż suma leveli elfów, to wtedy
# prezent nie zostanie utworzony.
# Jeśli jednak elfy poziadają większą sume leveli, to prezent zostanie utworzony i
# przybędzie `gift dificulty * 3` ChristmasCoinów.
# ##### Bonusowe punkty
# Podczas tworzenia prezentu wylosuj liczbe od 0 do 30, jeśli wylosowana liczba to 7, to pomnóż sume siły elfów o 3
def make_gift():
    global christmasCoin
    global list_of_elves
    gift_name = input("Podaj nazwę prezentu: ")
    level_of_difficulty = int(input('Podaj poziom trudności wyprodukowania prezentu'))
    random_num = random.randint(0, 30)

    sum = 0
    for i in list_of_elves:
        sum = int(i.level) + sum
    print(sum)

    if sum >= level_of_difficulty:
        print("Udało się stworzyć prezent")
        gift = Gift(gift_name, level_of_difficulty)
        list_of_gifts.append(gift)
        christmasCoin += level_of_difficulty * 3
        print("Zdobyłeś nowe ChristmasCoiny: ", level_of_difficulty * 3, "Lącznie masz teraz ", christmasCoin,
          "Christmascoinów")
    elif sum < level_of_difficulty:
        print('Twoje elfy mają za niski level na wytworzenie prezentu. '
              'Zobaczmy, czy mimo to uda się utworzyć prezent dzięki dodatkowej mocy.')
        if random_num == 7:
            sum = sum * 3
            print('Twoja dodatkowa moc sprawiła, że elfy mają łącznie level', sum)
            if sum >= level_of_difficulty:
                print('Masz szczęście i udało się wytworzyć prezent!')
                gift = Gift(gift_name, level_of_difficulty)
                list_of_gifts.append(gift)
                christmasCoin += level_of_difficulty * 3
                print("Zdobyłeś nowe ChristmasCoiny: ", level_of_difficulty * 3, "Lącznie masz teraz ", christmasCoin,
                      "Christmascoinów")
        else:
            print('Niestety, nie udało się utworzyć prezentu, Spróbuj ulepszyc swoje elfy.')
    menu()


# ##### Lista wyprodukowanych prezentów
# Zwróci liste wyprodukowanych prezentów
def print_gifts():
    for gift in list_of_gifts:
        print(str(gift) + "\n")
    menu()


# ##### Ulepszanie Elfa
# Ta opcja umożliwia ulepszenie elfa o 1 poziom.
# Ulepszenie kosztuje
# ```
# 1 - 4lvl   10 ChristmasCoinów
# 5 - 10lvl  15 ChristmasCoinów
# ```
# ##### Bonusowe Poziomy elfa
# Podczas ulepszania elfa wylosuj liczbe od 0 do 10. Jeśli ta liczba jest równa 7 to ulepsz elfa o 3 poziomy.
def upgrade_elf():
    global list_of_elves
    lucky_number = random.randint(5, 8)
    upgrade_name = input("Podaj imię elfa, którego chcesz ulepszyć: ")
    isfound = False

    for n in list_of_elves:
        if n.name == upgrade_name and lucky_number == 7:
            n.level += 3
            print("Udało ci się ulepszyć elfa ", n.name, "jego level to:", n.level)
            isfound = True

        elif n.name == upgrade_name and lucky_number != 7:
            print("Nie udało się ulepszyć elfa ", n.name, "jego level to wciąż ", n.level)
            isfound = True

    if isfound == False:
        print('Nie znaleziono elfa o imieniu ', upgrade_name)

    menu()


def menu():
    print("""
    ==================FABRYKA ELFÓW====================
    1. Dodaj Elfa
    2. Wyświetl Elfy
    3. Ulepsz Elfa
    4. Wyprodukuj prezent
    5. Lista wytworzonych prezentów
    """)
    #    suma_skrzatow()
    choice = int(input())
    if choice == 1:
        add_elf()
    if choice == 2:
        print_elves()
    if choice == 3:
        upgrade_elf()
    if choice == 4:
        make_gift()
    if choice == 5:
        print_gifts()


menu()
