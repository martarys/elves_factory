class Gift:
 #konstruktor
    def __init__(self, name, level_of_difficulty):
        self.name = name
        self.level_of_difficulty = level_of_difficulty

 # metoda pozwalająca na rzutowanie obiektu do stringa
    def __str__(self):
        return 'nazwa prezentu: ' + self.name + "; poziom trudności: " + str(self.level_of_difficulty)